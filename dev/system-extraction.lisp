(in-package #:asdf-install-tester)

;;; ---------------------------------------------------------------------------
;;; ASDF system metadata
;;; ---------------------------------------------------------------------------

(defparameter *known-licence-types*
    '((:bsd            "BSD or derivitive without the advertising clause")
      (:bsd-orig       "The 'original' BSD license which had the GPL incompatible advertising clause")
      (:gpl            "Gnu Public License")
      (:lpgl           "Lesser Gnu Public License")
      (:llgpl          "Lisp Lesser Gnu Public Licence (LGPL + Franz's preamble)")
      (:bugroff        "Bugroff license")
      (:public-domain  "In the public domain")
      (:x11            "Similar to :bsd. Probably equivalent to the :mit license.")
      (:mit            "Similar to :bsd. Probably equivalent to the :x11 license.")))

(defparameter *release-status*
    '((:alpha          "Not really usable except by author")
      (:beta           "Basically usable but not quite ready for production use.")
      (:stable         "As good as it's gonna get.")))

(defparameter *activity*
    '((:vapor          "Someone has proposed such a library but no code is available")
      (:active         "Someone is actively working on it, code available in some form")
      (:done           "Code available but no one is working on it anymore because it's done.")
      (:abandoned      "Code available but abandoned by original developers.")))

(defparameter *project-size*
    '((:solo           "Lone hacker")
      (:group          "Core group of cooperating developers")))

(defparameter *packaging*
    '((:l3             "L3-approved packaging")
      (:tar-gz         "Gzipped tar file")
      (:tar-bz2        "Bzip2'd tar file.")
      (:zip            "ZIP file.")
      (:lispsrc        "Single file of Lisp source code.")
      (:cvs            "Public CVS server")
      (:arch           "Public Arch repository")
      (:subversion     "Public subversion server")))

(defparameter *l3-librarian*
    '((:harley         "Harley Gorrell")
      (:miles          "Miles Egan")
      (:peter          "Peter Seibel")))

(defparameter *fields* 
    `((:name           "Human readable name of the library. Needn't be unique."      :public t :type string)
      (:version        "Version number of latest available version."                 :public t :type string)
      (:description    "One line, human-readable description of purpose of library." :public t :type string)
      (:author         "Name of author, if available."                               :public t :type string)
      (:author-email   "Contact email for author, if available."                     :public t :type string)
      (:license        "License library is distributed under."                       :public t :type keyword :values ,*known-licence-types* :open t)
      (:download-url   "URL where library can be downloaded"                         :public t :type string)
      (:homepage-url   "Homepage URL for project or author."                         :public t :type string)
      (:release-status "Status of software."                                         :public t :type keyword :values ,*release-status*)
      (:activity       "Level of (observable) activity on project."                  :public t :type keyword :values ,*activity*)
      (:runs-on        "Five-element tuple listing lisp/lisp-version/os/arch/ffi"    :public t :type runs-on-tuple :multiple t)
      (:ffi            "List of FFI types supported (or NIL for no FFI needed)"      :public t :type keyword :multiple t)
      (:project-size   "Number of folks working on project."                         :public nil :type keyword :values ,*project-size*)
      (:packaging      "Available download formats."                                 :public nil :type keyword :values ,*packaging* :multiple t :open t)
      (:packages       "Lisp packages used."                                         :public nil :type keyword :multiple t)
      (:l3-librarian   "Person who originally created this entry."                   :public nil :type keyword :values ,*l3-librarian* :open t)
      (:notes          "Notes about this library. Not to be published."              :public nil :type string :multiple t)

      (:license "what it is")
      (:depends-on "what is was")))

(defun public-fields ()
  (mapcar #'first (remove-if-not #'public-p *fields*)))

(defun public-p (field)
  (getf field :public))


;;; ---------------------------------------------------------------------------
;;; find-defsystem
;;; ---------------------------------------------------------------------------

(defgeneric find-defsystem (thing)
  (:documentation 
   "Reads an asd file and returns the first defsystem declaration with default values
  for missing properties. Since we don't want to screw around with evaling
  in-package forms, etc. we look for the defsystem using string=."))

;;; ---------------------------------------------------------------------------

(defmethod find-defsystem ((string string))
  (find-defsystem (make-string-input-stream string)))

;;; ---------------------------------------------------------------------------

(defmethod find-defsystem ((pathname pathname))
  (with-open-file (in pathname
                      :direction :input)
    (find-defsystem in)))

;;; ---------------------------------------------------------------------------

(defmethod find-defsystem ((stream stream))
  (let ((*package* (find-package :asdf)))
    (loop with stream = stream
       for form = (ignore-errors (read stream nil :eof)) 
       until (eq form :eof)
       when (and (consp form)
		 (eql (car form) 'asdf:defsystem)) return form)))

;;; ---------------------------------------------------------------------------
;;; write out properties you like...
;;; ---------------------------------------------------------------------------

(defun write-out-system-properties (system)
  (let* ((system-file
	  (make-pathname :name (string-downcase (symbol-name system))
			 :type "asd"
			 :defaults
			 (merge-pathnames 
			  (make-pathname :directory '(:relative "site-systems"))
			  *working-directory*)))
	 (system-def (when (probe-file system-file)
		       (find-defsystem system-file))))
    (when system-def
      (loop for prop in *fields* do
	   (when (getf system-def (first prop))
	     (write-status-property system (first prop)
				    (getf system-def (first prop)))))))
  ;; and the latest file date
  (multiple-value-bind (date file)
      (find-latest-system-file-date system)
    (when date
      (write-status-property 
       system :latest-file-date (date/time->string date))
      (write-status-property system :latest-file 
			     (format nil "~A" (namestring file)))))
  #+No
  ;; the system we care about isn't loaded...
  (multiple-value-bind (date file)
      (latest-system-file-date system)
    (when file
      (write-status-property system :latest-file-date (date/time->string date))
      (write-status-property system :latest-file 
			     (format nil "~S" (namestring file))))))

#+Test
(write-out-system-properties 'cl-containers)

(defun find-latest-system-file-date (system)
  (let* ((system-file
	  (probe-file  
	   (make-pathname :name (ensure-string system)
			  :type "asd"
			  :directory `(,@(pathname-directory
					  *working-directory*)
					 "site-systems")
			  :defaults *working-directory*)))
	 (system-directory 
	  (and system-file
	       (make-pathname :name nil
			      :type nil 
			      :directory (pathname-directory system-file)
			      :defaults system-file))))
    (when system-directory
      (multiple-value-bind (date file)
	  (latest-source-file-date system-directory)
	(when file
	  (values date file))))))
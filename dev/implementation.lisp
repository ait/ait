(in-package #:asdf-install-tester)

(defgeneric implementation-specific-quit (implementation)
  (:documentation "Returns the form that will 'quit' the implementation.")
  (:method ((implementation t))
           (error "Don't know how to quit from ~A" implementation)))

(defgeneric make-start-lisp-for-test-command (implementation)
  (:documentation  "Returns a string that can be passed to shell-command to start lisp and load the file 'startup.lisp'."))

(defmethod make-start-lisp-for-test-command ((implementation t))
  (error "No make-start-lisp-for-test-command defined for ~A" 
	 implementation))

(defgeneric implementation-specific-asdf-loader (implementation)
  (:documentation "Returns a string to be inserted into the startup file. The string should contain the commands necessary to load ASDF and ASDF-Install for the implementation."))

(defmethod implementation-specific-asdf-loader ((implementation t))
  ;; generic version
  (format nil 
	  "
(require :asdf)
(pushnew ~S asdf::*central-registry*)
(asdf:oos 'asdf:load-op 'asdf-install)
" (namestring (truename *asdf-install-directory*))))


;; this next bit taken from KRMCL (and then hacked without mercy)
;;;; *************************************************************************
;;;;
;;;; Name:          os.lisp
;;;; Purpose:       Operating System utilities
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  Jul 2003
;;;;
;;;; *************************************************************************

(defun shell-command (command &key (timeout most-positive-fixnum))
  "Synchronously execute command using a Bourne-compatible shell, 
returns the result of the command. If timeout is non-nil, it is used..."
  (declare (ignorable timeout))
  #+sbcl
  (let* ((process (sb-ext:run-program  
		   "/bin/sh"
		   (list "-c" command)
		   :input nil
		   :output nil 
		   :error nil)))
    (values
     (sb-impl::process-exit-code process)))    
  
  #+(or cmu scl)
  (let* ((process (ext:run-program  
		   "/bin/sh"
		   (list "-c" command)
		   :input nil :output nil :error nil)))
    (values
     (ext::process-exit-code process)))
  
  #+allegro
  (sys:with-timeout (timeout 
		     (progn
		       (error 'timeout-error :command command)))
    (multiple-value-bind (output error status)
	                 (excl.osi:command-output command :whole t)
      (values status)))
  
  #+lispworks
  (let ((status 
         (system:call-system-showing-output
          command
          :prefix ""
          :show-cmd nil
          :output-stream output)))
    (values status))
  
  #+clisp		
  (values
   (ext:run-shell-command  command :output :terminal :wait t))
  
  #+openmcl
  (let ((process (ccl:run-program  
		  "/bin/sh"
		  (list "-c" command)
		  :input nil 
		  :output nil
		  :error nil
		  :wait nil))
	(status nil)
	(exit-code nil))
    (ccl:process-wait-with-timeout
     "WAITING"
     (* ccl:*ticks-per-second* timeout)
     (lambda ()
       (setf (values status exit-code) 
	     (ccl:external-process-status process))
       (not (eq status :running))))
    (if (eq status :running)
	(progn
	  (error 'timeout-error :command command))
	(values exit-code)))
  
  
  #-(or openmcl clisp lispworks allegro scl cmu sbcl)
  (error " not implemented for this Lisp")
  )

(defun delete-directory (directory-spec)
  (unless (directory-pathname-p directory-spec)
    (setf directory-spec 
	  (make-pathname
	   :name nil
	   :type nil
	   :directory 
	   (append (pathname-directory directory-spec)
		   (list (namestring
			  (make-pathname 
			   :name (pathname-name directory-spec)
			   :type (pathname-type directory-spec)))))
	   :defaults directory-spec)))
  (shell-command 
   (format nil "rm -rf \"~A\"" (translate-logical-pathname directory-spec))))
(defpackage #:lisp-implementation
  (:use #:common-lisp)
  (:nicknames #:li)
  (:export
   #:lisp-version-string
   #:first-implementation
   #:first-architecture
   #:first-os
   #:implementation-specific-shorthand))
(in-package #:lisp-implementation)


;;; ---------------------------------------------------------------------------
;;; this next bit of code stolen from SWANK / SLIME
;;; ---------------------------------------------------------------------------

(defparameter *implementation-features*
  '(:allegro :lispworks :sbcl :openmcl :cmu :clisp :ccl
    :corman :cormanlisp :armedbear :gcl :ecl))

(defparameter *os-features*
  '(:macosx :linux :windows :mswindows :win32 :solaris :darwin :sunos :unix :apple))

(defparameter *architecture-features*
  '(:powerpc :ppc :x86 :x86-64 :i686 :pc386 :iapx386 :sparc))

(defun lisp-version-string ()
  #+cmu       (substitute #\- #\/ (lisp-implementation-version))
  #+sbcl      (lisp-implementation-version)
  #+ecl       (lisp-implementation-version)
  #+gcl       (let ((s (lisp-implementation-version))) (subseq s 4))
  #+openmcl   (format nil "~d.~d"
                      ccl::*openmcl-major-version* 
                      ccl::*openmcl-minor-version*)
  #+lispworks (lisp-implementation-version)
  #+allegro   (concatenate 'string (if (eq 'h 'H) "A" "M") 
                           excl::*common-lisp-version-number*)
  #+clisp     (let ((s (lisp-implementation-version)))
                (subseq s 0 (position #\space s)))
  #+armedbear (lisp-implementation-version)
  #+cormanlisp (lisp-implementation-version)
  #+digitool   (subseq (lisp-implementation-version) 8))

(defun first-in-features (features)
  (loop for f in features when
       (find f *features*) return it))

(defun first-implementation ()
  (first-in-features *implementation-features*))

(defun first-os ()
  (first-in-features *os-features*))

(defun first-architecture ()
  (first-in-features *architecture-features*))

(defun implementation-specific-shorthand ()
  "Return a name that can be used as a directory name that is
unique to a Lisp implementation, Lisp implementation version,
operating system, and hardware architecture."
  (flet ((maybe-warn (value fstring &rest args)
	   (cond (value)
		 (t (apply #'warn fstring args)
		    "unknown"))))
    (let ((lisp (maybe-warn (first-implementation)
			    "No implementation feature found in ~a." 
			    *implementation-features*))
	  (os   (maybe-warn (first-os)
			    "No os feature found in ~a." *os-features*))
	  (arch (maybe-warn (first-architecture)
			    "No architecture feature found in ~a."
			    *architecture-features*))
	  (version (maybe-warn (lisp-version-string)
			       "Don't know how to get Lisp ~
                                          implementation version.")))
      (format nil "~(~@{~a~^-~}~)" lisp version os arch))))

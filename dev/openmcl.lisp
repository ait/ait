(in-package #:asdf-install-tester)

(defmethod  implementation-specific-quit ((implementation (eql :openmcl)))
  "(ccl:quit)")

(defmethod make-start-lisp-for-test-command ((implementation (eql :openmcl)))
  (format nil " --no-init --load \"~A\" --eval \"(quit)\" "
          (translate-logical-pathname 
           (make-working-pathname "startup" "lisp"))))


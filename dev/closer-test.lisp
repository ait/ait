(in-package #:asdf-install-tester)

(let ((*systems-to-remove-each-time* '(lw-compat
				       contextl
				       closer-mop
				       mop-features))
      (*systems-to-test* '(lw-compat
			   contextl
			   closer-mop
			   mop-features)))
(main))


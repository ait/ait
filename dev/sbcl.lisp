(in-package #:asdf-install-tester)

(defmethod  implementation-specific-quit ((implementation (eql :sbcl)))
  "(sb-ext:quit)")

(defmethod make-start-lisp-for-test-command ((implementation (eql :sbcl)))
  (format nil " --noinform --noprint --disable-debugger --load \"~A\" --eval \"(quit)\""
          (translate-logical-pathname 
           (make-working-pathname "startup" "lisp"))))


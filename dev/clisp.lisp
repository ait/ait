(in-package #:asdf-install-tester)

(defmethod  implementation-specific-quit ((implementation (eql :clisp)))
  "(ext:quit)")

(defmethod make-start-lisp-for-test-command ((implementation (eql :clisp)))
  (format nil " \"~A\""
          (translate-logical-pathname 
           (make-working-pathname "startup" "lisp"))))

(defmethod implementation-specific-asdf-loader ((implementation (eql :clisp)))
  (format nil 
	  "
\(load ~S\)
\(pushnew ~S asdf::*central-registry*\)
\(asdf:oos 'asdf:load-op 'asdf-install\)
" (namestring (truename (make-pathname 
               :name "asdf"
               :type "lisp"
               :defaults *asdf-directory*)))
  (namestring (truename *asdf-install-directory*))))


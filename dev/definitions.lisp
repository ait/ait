(in-package #:asdf-install-tester)

(define-condition timeout-error (error)
  ((command :initarg :command :initform nil :reader command))
  (:report (lambda (c s)
	     (format s "Process timeout: command ~A" 
		     (command c)))))

(defvar *asdf-install-tester-directory*
  (slot-value (asdf:find-system 'asdf-install-tester)
	      'asdf::relative-pathname))

(defvar *lisp-implementations* nil)

(defvar *asdf-install-directory*
  (make-pathname 
   :name nil
   :type nil
   :defaults *asdf-install-tester-directory*
   :directory `(,@(pathname-directory
		   *asdf-install-tester-directory*)
		  "asdf-install")))

(defvar *asdf-directory*
  (make-pathname 
   :name nil
   :type nil
   :defaults *asdf-install-tester-directory*
   :directory `(,@(pathname-directory
		   *asdf-install-tester-directory*)
		  "asdf-install")))

(defparameter *working-directory*
  #+DIGITOOL
  ;;?? ugh -- GWK defines this in his MCL init.lisp file to point at 
  ;; #P"user-home:temporary;asdf-test;" Unfortunately, MCL points
  ;; user-homedir-pathname to the application directory, not the user's
  ;; homedir.
  #P"user-home:temporary;asdf-test;"
  #-DIGITOOL
  (make-pathname 
   :name nil
   :type nil
   :directory `(,@(pathname-directory (user-homedir-pathname))
		  "temporary" "asdf-test")
   :defaults (user-homedir-pathname))
  "Path to temporary working directory where all of the output, etc gets placed.")

(defparameter *message-type* "ait"
  "Somewhat silly solution to setting the extension of the output files.")

(defparameter *timeout* (* 6 60)
  "Number of seconds to give ASDF-Install to run.")

(defparameter *cliki-packages-url*
  "http://www.cliki.net/asdf-install-tester-packages-list")

(defparameter *systems-to-remove-each-time*
  :all
  "This can be either a list of specific systems to remove or the keyword :all to remove all of them.")

(defparameter *systems-to-test*
  :all
  "This can be a list of specific systems to test or the keyword :all to download the most recent list from *cliki-packages-url*.")

;; metabang systems
(defparameter *metabang-systems*
  '(
    ASDF-BINARY-LOCATIONS
    ASDF-INSTALL-TESTER
    ASDF-STATUS
    ASDF-SYSTEM-CONNECTIONS
    CL-CONTAINERS
    CL-GRAPH
    CL-HTML-PARSER
    CL-MATHSTATS
    CL-VARIATES
    CLNUPLOT
    DEFSYSTEM-COMPATIBILITY
    LIFT
    METABANG-BIND
    METABANG-DYNAMIC-CLASSES
    METATILITIES
    METATILITIES-BASE
    MOPTILITIES
    TINAA

    lw-compat
    closer-mop
    mop-features
    contextl
    portable-threads

    asdf-install
    ))




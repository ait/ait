(in-package #:common-lisp-user)

(defpackage #:asdf-install-tester
  (:use #:common-lisp #:cl-fad)
  (:nicknames #:ait)
  (:export 
   #:asdf-test
   #:timeout-error
   #:*asdf-install-directory*
   #:*timeout*
   #:*systems-to-remove-each-time*
   #:*systems-to-test*))


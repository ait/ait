(in-package #:asdf-install-tester)

(defun asdf-packages-list ()
  (destructuring-bind (response headers stream)
      (trivial-http-download:http-resolve 
       *cliki-packages-url*)
    (declare (ignore response headers))
    (unwind-protect
	 (let ((html (net.html.parser:parse-html stream)))
	   (sort 
	    (mapcar
	     (lambda (x)
	       (intern (string-upcase (cadadr x))))
	     (rest
	      (find :ul (third (second html)) :key (lambda (x) (if (consp x) (first x) x)))))
 #'string-lessp))
      (close stream))))
  
#+Example
(trivial-http-download:http-download
 "http://www.cliki.net/asdf-install-tester-packages-list")

(defun url->lml (url)
  (destructuring-bind (response headers stream)
      (trivial-http-download:http-resolve url)
    (declare (ignore response headers))
    (unwind-protect
	 (let ((html (net.html.parser:parse-html stream)))
	   html)
      (close stream))))

#+Ignore
;; example of a page retrieved when the revision number doesn't exist
((:!DOCTYPE " HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"") 
 (:HTML 
  (:HEAD 
   (:TITLE "CLiki : SLIME Tips")
   ((:META :NAME "ROBOTS" :CONTENT "NOINDEX,NOFOLLOW")) 
   ((:LINK :REL "alternate" :TYPE "application/rss+xml" :TITLE "Recent Changes" :HREF "http://www.cliki.net/recent-changes.rdf")) 
   ((:LINK :REL "stylesheet" :HREF "http://www.cliki.net/admin/cliki.css")))
  (:BODY
   ((:FORM :ACTION "http://www.cliki.net/admin/search") 
    ((:DIV :ID "banner")
     ((:SPAN :CLASS "search") ((:INPUT :NAME "words" :SIZE "30")) ((:INPUT :TYPE "submit" :VALUE "search")))
     ((:A :TITLE "CLiki home page" :CLASS "logo" :HREF "http://www.cliki.net/") "CL" ((:SPAN :CLASS "sub") "iki")) 
     "the common lisp wiki" :BR
     ((:DIV :ID "navbar")
      ((:A :HREF "http://www.cliki.net/index") "Home")
      ((:A :HREF "http://www.cliki.net/Recent%20Changes") "Recent Changes")
      ((:A :HREF "http://www.cliki.net/CLiki") "About CLiki")
      ((:A :HREF "http://www.cliki.net/Text%20Formatting") "Text Formatting")
      ((:A :ONCLICK "if(name=window.prompt('New page name ([A-Za-z0-9 ])')) document.location='http://www.cliki.net/edit/'+name ;return false;" :HREF "#") "Create New Page")
      ))) (:H1 "SLIME Tips"))))
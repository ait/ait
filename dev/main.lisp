(in-package #:asdf-install-tester)

(defun remove-systems (&key (systems *systems-to-remove-each-time*))
  (cond ((eq *systems-to-remove-each-time* :all)
	 (loop for directory in '("systems" "site-systems" "site") do
	      ;; mine seems more stable than cl-fad's
	      (delete-directory
	       (make-working-pathname directory nil)
	       )))
	(t
	 (dolist (system systems)
	   (remove-1-system system)))))

(defun remove-1-system (system)
  (setf system (etypecase system
                 (symbol (symbol-name system))
                 (string system)))

  (let* ((system-file (probe-file
		       (make-pathname 
			:name system
			:type "asd"
			:directory `(,@(pathname-directory 
					(make-working-pathname "dummy" "dummy"))
				       "systems"))))
         (site-wild (and system-file 
                         (make-pathname 
                          :name :wild
                          :type :wild
                          :directory (append (pathname-directory system-file)
                                             (list :wild-inferiors))))))
    (when site-wild
      (dolist (file (apply #'directory site-wild 
			   #+Ignore *implementation-directory-keys*
			   nil))
        (when (probe-file file)
          (delete-file file))))))

;;; ---------------------------------------------------------------------------

(defun asdf-test (&key (systems *systems-to-test*) 
	     (lisps (list (li:first-implementation)))
	     (documentation-root nil))
  "Test the systems in systems against each Lisp implementation in lisps. 
The keyword parameter systems can by a list of system names or the keyword
:all. In the later case, the current list of systems will be pulled from the 
CLiki. It defaults to the value of *systems-to-test*. The keyword parameter
:lisps should be a list of implementation names as keywords. It defaults 
to the return value of the li:first-implementation \(which ought to be 
the lisp currently running\).

Each system is tested against each lisp using the following steps:

1. A file 'startup.lisp' is created in a sub-directory of the working 
directory named after the lisp being used \(e.g., 'sbcl' for :sbcl\). 

2. A new process is created which starts the child lisp using
startup.lisp to initialize it. 

3. The startup.lisp contains the commands necessary to use ASDF-Install to
pull the system being tested and compile it. All output from the lisp process
will be saved."
  (when (eq systems :all)
    (setf systems (asdf-packages-list)))
  (let ((*message-type* "summary"))
    (write-status-message 'general "" t)
    (write-status-message 'general "\(" nil)
    (write-status-property 'general :start (date/time->string) nil)
    (write-status-property 'general :host-lisp (li:first-implementation) nil)
    (write-status-property 'general :os (li:first-os) nil)
    (write-status-property 'general :lisps lisps nil)
    (write-status-property 
     'general :architecture (li:first-architecture) nil)
    (write-status-property 'general :systems systems nil))
  (unwind-protect
       (progn
	 (loop for lisp in lisps 
	    for first? = t then nil do
	      (remove-non-lisp-files-from-asdf-install-directory)
	      (let ((*working-directory* 
		     (make-pathname 
		      :directory `(,@(pathname-directory *working-directory*)
				     "input"
				     ,(string-downcase 
				       (symbol-name lisp)))
		      :defaults *working-directory*)))
		(ensure-directories-exist *working-directory*)

		;; why is this unwind-protect here??
		(unwind-protect
		     (progn
		       (dolist (system systems)
			 (handler-case 
			     (test-system system lisp 
					  (and first? documentation-root))
			   (error (c)
			     (let ((*message-type* "summary"))
			       (write-status-message 
				'general 
				(format nil 
					"Error installing ~A (~A)" 
					system (fix-string (format nil "~A" c)))
				nil))))))))))
    ;; cleanup
    (let ((*message-type* "summary"))
      (write-status-property 'general :end (date/time->string) t)
      (write-status-message 'general "\)" t))))

;;; ---------------------------------------------------------------------------
  
(defun test-system (system lisp documentation-root)
  (remove-systems)
  (try-to-install system lisp documentation-root))

;;; ---------------------------------------------------------------------------

(defun ensure-string (thing)
  (etypecase thing
    (symbol (string-downcase (symbol-name thing)))
    (string thing)))

(defun fix-string (s)
  ;; dumb, but...
  (let ((ch-quote (code-char 34))
	(ch-slash (code-char 92)))
    (coerce (loop for ch across s nconc
		 (cond ((eq ch ch-quote) (list ch-slash ch-quote))
		       (t (list ch))))
	    'string)))

;;; ---------------------------------------------------------------------------

(defun make-working-pathname (name type)
  (make-pathname 
   :name (and name (string-downcase (ensure-string name)))
   :type (and type (string-downcase (ensure-string type)))
   :defaults *working-directory*))

;;; ---------------------------------------------------------------------------

(defun date/time->string (&optional (datetime (get-universal-time)))
  (multiple-value-bind (second minute hour day month year day-of-the-week)
      (decode-universal-time datetime)
    (declare (ignore day-of-the-week))
    (format nil "~D-~2,'0D-~2,'0D ~2,'0D:~2,'0D:~2,'0D"
	    year month day hour minute second)))

;;; ---------------------------------------------------------------------------

(defun try-to-install (system lisp-id &optional documentation-root)
  (let ((lisp-parameters (find-lisp-implementation lisp-id)))
    (make-startup-file system (second lisp-parameters) 
		       :documentation-root documentation-root)
    (remove-status-file system)
    (write-status-message system (format nil "\(:install \"~(~A~)\" " system) nil)
    (write-status-property system :start (date/time->string))
    (write-status-property system :lisp lisp-id nil)

    ;; these next are of the hosting lisp, not the testing lisp
    (write-status-property 
     system :host-lisp-version  (li:lisp-version-string) nil)
    (write-status-property
     system :host-implementation (li:implementation-specific-shorthand) nil)

    (let ((command 
	   (concatenate 'string (third lisp-parameters)
			(make-start-lisp-for-test-command (second lisp-parameters))))
	  (start-time (get-universal-time)))
      (handler-case 
	  (let ((result (shell-command command :timeout *timeout*)))
	    (format t "~%~30,A: ~A" system result)
	    (write-status-property system :result result))
	(timeout-error ()
	  (write-status-property system :error 
				 (format nil "Timeout Error (~A)" *timeout*)))
	(error (c)
	  (write-status-property system :internal-error 
				 (fix-string (format nil "~A" c)))))
      (write-out-system-properties system)
      (write-status-property system :end (date/time->string))
      (write-status-property system :elapsed-time (- (get-universal-time)
						     start-time)))
    (write-status-message system ")" nil)))

;;; ---------------------------------------------------------------------------

(defun remove-status-file (system)
  (let ((file (make-working-pathname system "ait")))
    (when (probe-file file)
      (delete-file file))))

(defun write-status-property (system name value &optional (newline? t))
  (write-status-message 
   system (format nil "~(:~A~) ~S " name value) newline?))

(defun write-status-message (system message &optional (newline? t))
  (format *standard-output* "~A" message)
  (when newline?
    (terpri *standard-output*))
  (force-output *standard-output*)
  (with-open-file (status (make-working-pathname system *message-type*)
			  :direction :output
			  #+CLISP :buffered #+CLISP nil
			  :if-exists :append
			  :if-does-not-exist :create)
    (format status "~A" message)
    (when newline?
      (terpri status))
    (force-output status)))

;;; ---------------------------------------------------------------------------

(defun document-system-with-tinaa (system output)
  "Return a string with the code needed to load tinaa and document system."
  (format nil "
~{(load ~S)~%~}
(asdf:oos 'asdf:load-op 'tinaa)
(funcall (find-symbol (symbol-name (read-from-string \"document-system\")) :tinaa)
         (find-symbol (symbol-name (read-from-string \"asdf-system\")) :tinaa) '~S ~S)"
	  (and (asdf:find-system 'tinaa nil)
	       (every (lambda (s) (asdf:find-system s nil))
		      (dsc:collect-system-dependencies 'tinaa))
	       (mapcar (lambda (s)
			 (translate-logical-pathname (dsc:system-source-file s)))
		       (append (dsc:collect-system-dependencies 'tinaa) 
                               (list 'tinaa))))   
	  system
	  (translate-logical-pathname output)))

;;; ---------------------------------------------------------------------------

(defun make-startup-file (system lisp &key documentation-root)
  (let ((*package* (find-package 'asdf-install-tester))
	(startup-file (make-working-pathname "startup" "lisp"))
	(documentation-root 
	 (when documentation-root
	   (merge-pathnames
	    (make-pathname 
	     :directory `(:relative ,(string-downcase (symbol-name system)))) 
	    documentation-root))))
    (with-open-file (out startup-file
                         :direction :output
                         :if-exists :supersede
                         :if-does-not-exist :create)
      
      (format out 
              "
(in-package #:common-lisp-user)

~A

;; implementation determination
(load ~S)

(let* ((system '~S)
       (system-name (string-downcase (symbol-name system)))
       (working \"~A\")
       ;; don't care about keys
       (asdf-install::*verify-gpg-signatures* nil)
       ;; only use our own location
       (asdf-install::*locations*
        `((,(merge-pathnames (make-pathname :directory '(:relative \"site\"))
                             working)
           ,(merge-pathnames (make-pathname :directory '(:relative \"site-systems\"))
                             working)
           \"temporary install\")))
       ;; set a location
       (asdf-install::*preferred-location* 0)
       ;; ASDF registry
       (asdf::*central-registry* (list (second (first asdf-install::*locations*))))
       ;(*load-verbose* t)
       ;(*compile-verbose* t)
       (ch-quote (code-char 34))
       (ch-slash (code-char 92)))

  #+SBCL
  (setf asdf::*central-registry*
	(append asdf::*central-registry*
		(list 
		 (MERGE-PATHNAMES \"systems/\" (TRUENAME (POSIX-GETENV \"SBCL_HOME\"))))))

  (flet ((fix-string (s)
	   ;; dumb, but...
	   (coerce (loop for ch across s nconc
		      (cond ((eq ch ch-quote) (list ch-slash ch-quote))
			    (t (list ch))))
		 'string)))
   
    (with-open-file (out (make-pathname 
				:name system-name
				:type \"ait\"
				:defaults working)
			 :direction :output
			 #+CLISP :buffered #+CLISP nil
			 :if-exists :append
			 :if-does-not-exist :create)
      (format out \"~~( :os ~~S :lisp-version ~~S :implementation ~~S ~~)\"
	      (li:first-os) (li:lisp-version-string)
	      (li:implementation-specific-shorthand)))

    (with-open-file (*standard-output* (make-pathname 
                                        :name system-name
				        :type \"output\"
				        :defaults working)
                                       :direction :output
                                       :if-exists :supersede
                                       :if-does-not-exist :create)
      (let ((*error-output* *standard-output*))
	(handler-case 
	    (progn
	      (asdf-install:install system)
	      ~(~A~))
	  (error (c)
	    (let* ((error-file (make-pathname 
				:name system-name
				:type \"ait\"
				:defaults working)))
	      (format *error-output* \"~~A\" c)
	      (force-output *error-output*)
	      (with-open-file (out error-file
				   :direction :output
				   #+CLISP :buffered #+CLISP nil
				   :if-exists :append
				   :if-does-not-exist :create)
		(format out \" :error ~~S \" 
			(fix-string (format nil \"~~A\" c)))
		(force-output out)))))))))
~A
"
              (implementation-specific-asdf-loader lisp) 
              (make-pathname 
	       :name "lisp-implementation"
	       :type "lisp"
	       :directory `(,@(pathname-directory *asdf-install-tester-directory*)
                              "dev")
	       :defaults *asdf-install-tester-directory*)
              system 
              (translate-logical-pathname *working-directory*)
              (when documentation-root
                (document-system-with-tinaa system documentation-root))
              (implementation-specific-quit lisp)))
    (values startup-file)))



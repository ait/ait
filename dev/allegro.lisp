(in-package #:asdf-install-tester)

(defmethod  implementation-specific-quit ((implementation (eql :allegro)))
  "(excl:exit)")

(defmethod make-start-lisp-for-test-command ((implementation (eql :allegro)))
  (format nil " -qq -L \"~A\" -kill"
          (translate-logical-pathname 
           (make-working-pathname "startup" "lisp"))))


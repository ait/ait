(in-package #:ait)

(defun url-header-date (url)
  (destructuring-bind (response headers &optional stream)
      (trivial-http-download:http-resolve url :signal-error? nil)
    (when stream (close stream))
    (cond ((member response '(200 201))
	   (let ((last-modified (cdr (find :last-modified headers :key #'car)))
		 (date (cdr (find :date headers :key #'car))))
	     (cond (last-modified
		    (values (date:parse-time last-modified) :last-modified))
		   (date
		    (values (date:parse-time date) :date))
		   (t
		    (values nil nil)))))
	  (t
	   (values nil response)))))

(defun package-date (package)
  (url-header-date (format nil "http://www.cliki.net/~A?download" package)))

(defun get-package-dates (&optional (systems (asdf-packages-list)))
  (loop for system in systems collect
       (multiple-value-bind (date how)
	   (package-date system)
	 (list system date how))))



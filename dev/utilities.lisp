(in-package #:asdf-install-tester)

(defun add-lisp-implementation (name lisp command)
  (remove-lisp-implementation name)
  (pushnew (list name lisp command) *lisp-implementations*
           :key #'first))

(defun remove-lisp-implementation (name)
  (setf *lisp-implementations*
	(remove name *lisp-implementations* :key #'first)))

(defun defined-lisp-implementations ()
  (mapcar #'first *lisp-implementations*))

(defun find-lisp-implementation (lisp-id)
  (find lisp-id *lisp-implementations* :key #'first))

(defun latest-file-date (directory &key (filter (constantly t)))
  (let ((latest-date nil) 
	(latest-file nil))
    (cl-fad:walk-directory 
     directory
     (lambda (file)
       (let ((file-date (file-write-date file)))
         (when (or (null latest-date)
                   (and file-date
                        (> file-date latest-date)))
           (setf latest-date file-date
                 latest-file file))))
     :test filter)
    (values latest-date latest-file)))

(defun latest-source-file-date (directory)
  (latest-file-date directory :filter #'probably-a-source-file-p))

(defun remove-binary-files-for-current-lisp (directory)
  "Maps over the files returned from #'walk-directory and deletes
any that have the same pathname-type as compile-file-pathname returns."
  (cl-fad:walk-directory
   directory
   #'delete-file
   :test (lambda (file)
	   (string-equal (pathname-type file) 
			 (pathname-type (compile-file-pathname file))))))

(defun remove-non-lisp-files-from-asdf-install-directory ()
  "Maps over the files in the asdf-install-directory and removes
any that aren't source \(e.g., lisp, asd. or without extension\)."
  (cl-fad:walk-directory 
   *asdf-install-directory*
   #'delete-file
   :test (complement #'probably-a-source-file-p)))

(defun probably-a-source-file-p (filename)
  (and (not (cl-fad:directory-pathname-p filename))
       (or (= (length (pathname-type filename)) 0)
	   (member (pathname-type filename) '("lisp" "asd" "asdf" "html")
		   :test #'string-equal))))

#+Ignore
(defun latest-system-file-date (system-name)
  (let ((latest-date 0) 
	(latest-file nil))
    (defsystem-compatibility::map-system-files
     system-name
     (lambda (file)
       (let ((file-date (or (file-write-date file) 0)))
	 (when (or (null latest-date)
		   (and file-date
			(> file-date latest-date)))
	   (setf latest-date file-date
		 latest-file file))))
     :include-pathname? t)
    (values latest-date latest-file)))

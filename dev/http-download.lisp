(defpackage trivial-http-download 
  (:use "COMMON-LISP" "TRIVIAL-HTTP")
  (:export #:http-download #:http-resolve #:download-error)
  (:documentation "Download files via HTTP."))
(in-package #:trivial-http-download)

;; from ASDF-Install
(define-condition download-error (error)
  ((url :initarg :url :reader download-url)
   (response :initarg :response :reader download-response))
  (:report (lambda (c s)
	     (format s "Server responded ~A for GET ~A"
		     (download-response c) (download-url c)))))

(defun http-resolve (url &key (http-method 'thttp:http-get)
		     (signal-error? t) (verbose? nil))
  "As with http-get, http-response returns the HTTP response, headers and stream. HTTP-response resolves 301 and 302 responses, signals an error on responses
greater than 400. If there is not an error, then the caller is responsible 
for closing the HTTP stream."
  ;; mostly from ASDF-Install
  (handler-case 
      (destructuring-bind (response headers stream)
	  (block got
	    (loop
	       (destructuring-bind (response headers &optional stream)
		   (funcall http-method url)
		 (when verbose? 
		   (format *debug-io* "~% ~A -> ~A" url response))
		 (unless (member response '(301 302))	       
		   (return-from got (list response headers stream)))
		 (when stream 
		   (close stream))
		 (setf url (cdr (assoc :location headers))))))
	(when (>= response 400)
	  (when stream (close stream))
	  (error 'download-error :url url :response response))
	(list response headers stream))
    (error (c)
      (unless signal-error?
	(signal c))
      (list nil nil nil))))

(defun http-download (url destination)
  ;; mostly from ASDF-Install
  (destructuring-bind (response headers stream)
      (http-resolve url)
    (declare (ignore response))
    (let ((length (parse-integer (or (cdr (assoc :content-length headers)) "")
				 :junk-allowed t)))
      #+:clisp (setf (stream-element-type stream)
		     '(unsigned-byte 8))
      (with-open-file (o destination :direction :output
			 #+(or :clisp :digitool (and :lispworks :win32))
			 :element-type
			 #+(or :clisp :digitool (and :lispworks :win32))
			 '(unsigned-byte 8)
			 #+SBCL
			 ::external-format 
			 #+SBCL :iso-8859-1
			 :if-exists :supersede)
	#+(or :cmu :digitool)
	(copy-stream stream o)
	#-(or :cmu :digitool)
	(if length
	    (let ((buf (make-array length
				   :element-type
				   (stream-element-type stream))))
	      #-:clisp (read-sequence buf stream)
	      #+:clisp (ext:read-byte-sequence buf stream :no-hang nil)
	      (write-sequence buf o))
	    (copy-stream stream o))))
    (close stream)))

;;; from ASDF-install
;; for non-SBCL we just steal this from SB-EXECUTABLE
#-(or :sbcl :digitool)
(defvar *stream-buffer-size* 8192)
#-(or :sbcl :digitool)
(defun copy-stream (from to)
  "Copy into TO from FROM until end of the input stream, in blocks of
*stream-buffer-size*.  The streams should have the same element type."
  (unless (subtypep (stream-element-type to) (stream-element-type from))
    (error "Incompatible streams ~A and ~A." from to))
  (let ((buf (make-array *stream-buffer-size*
			 :element-type (stream-element-type from))))
    (loop
     (let ((pos #-(or :clisp :cmu) (read-sequence buf from)
                #+:clisp (ext:read-byte-sequence buf from :no-hang nil)
                #+:cmu (sys:read-n-bytes from buf 0 *stream-buffer-size* nil)))
       (when (zerop pos) (return))
       (write-sequence buf to :end pos)))))

#+:digitool
(defun copy-stream (from to)
  "Perform copy and map EOL mode."
  (multiple-value-bind (reader reader-arg) (ccl::stream-reader from)
    (multiple-value-bind (writer writer-arg) (ccl::stream-writer to)
      (let ((datum nil))
        (loop (unless (setf datum (funcall reader reader-arg))
                (return))
              (funcall writer writer-arg datum))))))

#+:sbcl
(declaim (inline copy-stream))
#+:sbcl
(defun copy-stream (from to)
  (sb-executable:copy-stream from to))

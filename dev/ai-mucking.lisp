(in-package #:asdf-install)

#|
need to parse the dates
need to save the date of the current build somewhere

|#

(setf u2 "http://common-lisp.net/project/cl-containers/metabang.bind/metabang.bind_latest.tar.gz")

(url-connection u2)

(setf h2 (url-connection u2))

(cdr
 (find :last-modified (second h2) :key (lambda (x) (when (consp x) (car x)))))
(cdr (assoc :last-modified (second h2)))


(defun package-last-modified-date (package-name-or-url)
  (let ((url (if (= (mismatch package-name-or-url "http://") 7)
	         package-name-or-url
	         (format nil "http://www.cliki.net/~A?download"
		         package-name-or-url)))
        )
    (destructuring-bind (response headers stream)
	(block got
	  (loop
	   (destructuring-bind (response headers stream) (url-connection url)
	     (unless (member response '(301 302))	       
	       (return-from got (list response headers stream)))
	     (close stream)
	     (setf url (cdr (assoc :location headers))))))
      (declare (ignore stream))
      (when (>= response 400)
        (error 'download-error :url url :response response))
      (cdr (assoc :last-modified headers)))))
      
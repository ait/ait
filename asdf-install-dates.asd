#|

Author: Gary King

|#

(defpackage :asdf-install-dates-system (:use #:cl #:asdf))
(in-package :asdf-install-dates-system)

(defsystem asdf-install-dates
  :version "0.1"
  :author "Gary Warren King <gwking@metabang.com>"
  :maintainer "Gary Warren King <gwking@metabang.com>"
  :licence "MIT Style License"
  :description "Determine ASDF-Install tarball dates"
  :components ((:module "dev"
		        :components ((:file "package")
                                     (:file "http-download")
				     (:file "definitions"
   				             :depends-on ("package"))
                                     (:file "update-asdf-packages"
                                            :depends-on ("definitions" "http-download"))
				     (:file "retrieve-system-update-time"
				            :depends-on ("update-asdf-packages"))
					     )))
  :depends-on (trivial-http
               cl-html-parse
	       net-telent-date
	       cl-fad))



#|

Author: Gary King

|#

(defpackage :asdf-install-tester-system (:use #:cl #:asdf))
(in-package :asdf-install-tester-system)

(defsystem asdf-install-tester
  :version "0.3"
  :author "Gary Warren King <gwking@metabang.com>"
  :maintainer "Gary Warren King <gwking@metabang.com>"
  :licence "MIT Style License"
  :description "A tester for ASDF-Install"
  :components ((:module "dev"
		        :components ((:static-file "notes.text")
				     
                                     (:file "package")
				     (:file "lisp-implementation")
                                     (:file "definitions"
                                            :depends-on ("package"))
                                     (:file "implementation"
                                            :depends-on ("definitions"))
                                     (:file "http-download")
				     (:file "utilities"
    				            :depends-on ("package"))
                                     (:file "update-asdf-packages"
                                            :depends-on ("package"
					    "http-download"))
				     (:file "main"
      				     	    :depends-on ("definitions" 
                                                         "implementation"
							 "utilities"
							 "lisp-implementation"))
				     (:file "system-extraction"
				            :depends-on ("main"))
				     (:file "allegro"
				            :depends-on ("implementation"))
				     (:file "clisp"
				            :depends-on ("implementation"))
				     (:file "openmcl"
				            :depends-on ("implementation"))
				     (:file "sbcl"
				            :depends-on ("implementation"))
					     ))
               (:file "preferences" :depends-on ("dev"))
               (:module "website"
                        :components ((:module "source"
                                              :components ((:static-file "index.lml"))))))
  :depends-on (trivial-http
               cl-html-parse
	       cl-fad
	       defsystem-compatibility))


